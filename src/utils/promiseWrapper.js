
/**
 * 
 * @callback Promisable - async function or promise 
 * @param {Promise<Object|Error>} promiseData 
 */

/**
 * 
 * @param {Promisable} promiseable 
 * @returns {Promise<Array<Object|Error>>} [response, error]
 */
export default async function promiseWrapper(promise) {
    try {
        const response = await promise;
        return [response, null];
    } catch (error) {
        return [null, error];
    }
}