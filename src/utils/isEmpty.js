export default function isEmpty(obj) {
    for (const x in obj) if (Object.prototype.hasOwnProperty.call(obj, x)) return false;
    return true;
}