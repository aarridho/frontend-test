const isOnline = (checkIfClient = true) => {
    if (checkIfClient && typeof window === 'undefined') return false;

    return navigator.onLine;
};

export default isOnline;