const formatError = (message, data) => {
    let err = new Error(message);

    err.data = data;

    return err;
};

export default formatError;