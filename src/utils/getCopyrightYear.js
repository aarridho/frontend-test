export default function getCopyrightYear(productYear = 2021) {
    const currentYear = new Date().getFullYear();

    if (productYear === currentYear) return currentYear;
    return `${productYear} - ${currentYear}`;
}