import differenceInDays from 'date-fns/differenceInDays';

export default function normalizeDate(date) {
    const targetDate = new Date(date);
    const currentDate = new Date();
    const diff = differenceInDays(
        targetDate,
        currentDate
    );

    const currentYear = currentDate.getFullYear();
    const formatDate = (targetDate.getFullYear() - currentYear) === 0 ? 'd LLLL' : 'PPP';

    return { targetDate, currentDate, diff, currentYear, formatDate };
}