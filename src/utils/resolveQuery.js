/**
 * 
 * @typedef {{
 *  limit?: number,
 *  page?: number,
 *  search?: string,
 *  ...rest: any
 * }} Query 
 */

/**
 * 
 * @param {Query} param0 
 * @returns 
 */
export default function resolveQuery({ search, limit = 5, page = 1, ...query }) {
    /**
     * @type {Query}
     */
    let resolve_query = {
        ...query,
        limit: limit ?? 5,
        page: page ?? 0,
    };
    if (search) resolve_query.search = search;

    return resolve_query;
}