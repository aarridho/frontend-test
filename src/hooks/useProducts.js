import axios from '@lib/axios';
import isOnline from '@utils/isOnline';
import promiseWrapper from '@utils/promiseWrapper';
import QueryString from 'qs';
import { useCallback, /* useState */ } from 'react';
import /* useSWR, */ { useSWRConfig } from 'swr';
import useSWRInfinite from 'swr/infinite';

/**
 * @typedef {{
 *  page: Number,
 *  size: Number
 * }} Query
 */

/**
 * @typedef {{
 *  totalItems: Number,
 *  currentPage: Number,
 *  itemPerPage: Number,
 *  totalPages: Number
 * }} Meta 
 */

/**
 * @typedef {{
 *  last: String,
 *  next: String,
 *  self: String
 * }} Links 
 */

/**
 * @typedef {import('./useProduct.js').Product} Product
 */

/**
 * @type {String}
 */
export const PRODUCTS_PATH = '/api/v2/gifts';

/**
 * 
 * @param {Query} param0 
 * @returns 
 */
export const getProducts = async ({ page, size }) => {
    const query = QueryString.stringify({ 'page[number]': page, 'page[size]': size }, { encodeValuesOnly: true });
    const resolve_path = PRODUCTS_PATH + '?' + query;

    const request = axios.get(resolve_path, {
        headers: {
            'Content-Type': 'application/json',
        }
    });

    return await promiseWrapper(request);
};


/**
 * 
 * @param {Boolean} immediate
 * @param {Query} query 
 * @returns {{
 *  products: Array[]<Product>,
 *  meta: Meta,
 *  links: Links,
 *  error: Error,
 *  loading: Boolean,
 *  fetchProducts: Function,
 *  mutateProducts: Function,
//  *  handleProductsQuery: Function,
 * }}
 */


const useProducts = (immediate, defaultQuery) => {
    const { cache } = useSWRConfig();
    // const [query, setQuery] = useState(defaultQuery);
    const { page, size: limit } = defaultQuery;

    /**
     * 
     * @param {String} resource_path
     * @param {Number} page
     * @param {Number} size
     * @returns {(Promise<Object>|Error)}
     */
    const fetchData = useCallback(async (...key) => {
        const checkOnline = isOnline(true);
        if (!checkOnline) return cache.get(key);

        const [, page, size] = key;
        const [response, error] = await getProducts({ page, size });

        if (error) throw error;
        return response?.data;
    }, [cache]);

    // const { data, error, mutate, } = useSWR(immediate && page && size ? [PRODUCTS_PATH, page, size] : null, fetchData);

    const getKey = (pageIndex, previousPageData) => {
        if (previousPageData && previousPageData?.meta?.currentPage === previousPageData?.meta?.totalPages) return null; // reached the end
        return immediate ? [PRODUCTS_PATH, pageIndex + 1, limit] : null; // SWR key
    };
    const { data, error, mutate, size, setSize } = useSWRInfinite(getKey, fetchData, { initialSize: page });

    /**
     * 
     * @param {Query} query
     */
    // const handleQuery = useCallback(({ page, size }) => {
    //     setQuery({ page, size });
    // }, []);

    return {
        products: data || [],
        meta: data?.meta,
        links: data?.links,
        error,
        loading: !data && !error,
        fetchProducts: fetchData,
        mutateProducts: mutate,
        // handleProductsQuery: handleQuery,
        size,
        setSize,
    };
};

export default useProducts;