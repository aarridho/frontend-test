import { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import useSWR, { useSWRConfig } from 'swr';
import axios from '@lib/axios';
import isOnline from '@utils/isOnline';
import promiseWrapper from '@utils/promiseWrapper';

/**
 * @typedef {{
 *  id: Number,
 *  name: String,
 *  info: String,
 *  description: String,
 *  points: Number,
 *  slug: String,
 *  stock: Number,
 *  images: String[],
 *  isNew: Number,
 *  rating: Number,
 *  numOfReviews: Number,
 *  isWishlist: Number,
 * }} ProductAttributes
 */

/**
 * @typedef {{
 *  id: String,
 *  type: String,
 *  attributes: ProductAttributes
 * }} Product
 */

export const PRODUCT_PROPTYPES = PropTypes.shape({
    id: PropTypes.string,
    type: PropTypes.string,
    attributes: PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
        info: PropTypes.string,
        description: PropTypes.string,
        points: PropTypes.number,
        slug: PropTypes.string,
        stock: PropTypes.number,
        images: PropTypes.arrayOf(PropTypes.string),
        isNew: PropTypes.number,
        rating: PropTypes.number,
        numOfReviews: PropTypes.number,
        isWishlist: PropTypes.number,
    })
});

/**
 * @type {String}
 */
export const PRODUCT_PATH = '/api/v2/gifts';

/**
 * 
 * @param {Number} product_id 
 * @returns 
 */
export const getProductById = async (product_id) => {
    const resolve_path = PRODUCT_PATH + '/' + product_id;

    const request = axios.get(resolve_path, {
        headers: {
            'Content-Type': 'application/json',
        }
    });

    return await promiseWrapper(request);
};

/**
 * @param {Boolean} immediate
 * @param {Product} defaultData 
 * @returns {{
 *  product: Product,
 *  error: Error,
 *  loading: Boolean,
 *  fetchProduct: Function
 *  mutateProduct: Function
 * }}
 */

const useProduct = (immediate, defaultData) => {
    const { cache } = useSWRConfig();
    const [id, setId] = useState(defaultData?.id);

    /**
     * 
     * @param {String} resource_path 
     * @param {Number} product_id 
     * @returns {(Promise<Object>|Error)}
     */
    const fetchData = useCallback(async (...key) => {
        const checkOnline = isOnline(true);
        if (!checkOnline) return cache.get(key);

        const [, product_id] = key;
        const [response, error] = await getProductById(product_id);

        if (error) throw error;
        return response?.data?.data;
    }, [cache]);

    const { data, error, mutate, } = useSWR(immediate && id ? [PRODUCT_PATH, id] : null, fetchData, { fallbackData: defaultData });

    /**
     * 
     * @param {Number} product_id
     */
    const handleId = useCallback((product_id) => {
        setId(product_id);
    }, []);

    return {
        product: data || null,
        error,
        loading: !data && !error,
        fetchProduct: fetchData,
        mutateProduct: mutate,
        handleProductId: handleId,
    };
};

export default useProduct;