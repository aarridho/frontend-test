import axios from '@lib/axios';
import promiseWrapper from '@utils/promiseWrapper';
import { useCallback, useEffect, useState } from 'react';

const WISHLIST_PATH = '/api/v2/gifts';

/**
 * 
 * @returns {{
 *  postWishlist: Function
 * }}
 */

const useWishlist = (defaultWishlist) => {
    const [isWishlist, setIsWishlist] = useState(defaultWishlist || false);
    useEffect(() => {
        if (typeof defaultWishlist === 'boolean') setIsWishlist(defaultWishlist);
    }, [defaultWishlist]);

    const postWishlist = useCallback(async (gift_id) => {
        const resolve_path = WISHLIST_PATH + '/' + gift_id + '/wishlist';
        const request = axios.post(resolve_path);

        return await promiseWrapper(request);
    }, []);

    return {
        isWishlist,
        setIsWishlist,
        postWishlist,
    };
};

export default useWishlist;