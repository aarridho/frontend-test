import { useCallback, useState } from 'react';

const useCounter = (defaultCount) => {
    const [counter, setCounter] = useState(defaultCount);

    const increase = useCallback(() => {
        setCounter((prevCounter) => prevCounter + 1);
    }, []);

    const decrease = useCallback(() => {
        setCounter((prevCounter) => {
            if (prevCounter - 1 < 0) return prevCounter;
            return prevCounter - 1;
        });
    }, []);

    return { counter, increase, decrease };
};

export default useCounter;