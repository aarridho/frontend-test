import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './styles.module.css';

const WishlistButton = ({ className, ...props }) => {
    return (
        <button type="button" className={classNames('btn', styles.button__wishlist, className)} {...props}>
        </button>
    );
};

WishlistButton.propTypes = {
    className: PropTypes.string,
};

export default WishlistButton;