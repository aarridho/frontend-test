import PropTypes from 'prop-types';
import Link from 'next/link';
import Image from 'next/image';
import classNames from 'classnames';
import styles from './styles.module.css';

const NavbarComponent = () => {
    return (
        <nav className="navbar fixed-top navbar-expand-lg navbar-light bg-white">
            <div className={classNames('container', styles.navbar__container)}>
                <Link href="/">
                    <a className="navbar-brand m-0">
                        <Image src="/images/logo-dummy.png" alt="My Gifts Logo" priority width={161} height={67} />
                    </a>
                </Link>
            </div>
        </nav>
    );
};

NavbarComponent.propTypes = {
    sidebarToggler: PropTypes.func,
    user: PropTypes.shape({
        name: PropTypes.string,
        username: PropTypes.string,
        photo: PropTypes.string,
    }),
    handleLogout: PropTypes.func,
};

export default NavbarComponent;