import FooterComponent from '@components/FooterComponent';
import NavbarComponent from '@components/NavbarComponent';
import PropTypes from 'prop-types';
import styles from './styles.module.css';

const LayoutComponent = ({ children }) => {
    return (
        <div className={styles.layout__container}>
            <NavbarComponent />
            <div className={styles.main__content}>
                {children}
            </div>
            <FooterComponent />
        </div>
    );
};

LayoutComponent.propTypes = {
    children: PropTypes.node,
};

export default LayoutComponent;