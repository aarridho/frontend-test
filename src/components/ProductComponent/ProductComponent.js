import classNames from 'classnames';
import PropTypes from 'prop-types';
import styles from './styles.module.css';

const ProductComponent = ({ products, handleProducts }) => {
    return (
        <div className={styles.product__container}>
            {products.map((product, index) => (
                <div className={classNames('form-check', styles.product__group)} key={product.title} data-product-id={index}>
                    <label className={classNames('form-check-label', styles.product__label)} htmlFor={product.title}>{product.label}</label>
                    <input className={classNames('form-check-input', styles.product__input)} type="checkbox" value={product.isOn} id={product.title} onChange={handleProducts} />
                </div>
            ))}
        </div>
    );
};

ProductComponent.propTypes = {
    products: PropTypes.arrayOf(PropTypes.shape({
        title: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired,
        isOn: PropTypes.bool.isRequired,
    })),
    handleProducts: PropTypes.func.isRequired,
};

export default ProductComponent;