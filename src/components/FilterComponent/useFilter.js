import { useCallback, useState } from 'react';

/**
 * 
 * @typedef {{
 *  title: String,
 *  label: String,
 *  condition: Function,
 *  value_label: String,
 *  isOn: Boolean
 * }} Filter 
 */

/**
 * 
 * @param {Array<Filter>} defaultFilters 
 * @returns {{
 *  filters: Array<Filter>,
 *  handleFilters: Function
 * }}
 */

const useFilter = (defaultFilters) => {
    const [filters, setFiltersOn] = useState(defaultFilters);

    const handleFilters = useCallback((e) => {
        const filterId = parseInt(e.currentTarget?.dataset?.filterId);
        if (typeof filterId !== 'number') return;

        setFiltersOn((prevState) => {
            let newFiltersOn = [...prevState];

            newFiltersOn[filterId] = {
                ...newFiltersOn[filterId],
                isOn: !newFiltersOn[filterId].isOn
            };

            return newFiltersOn;
        });

    }, []);

    return { filters, handleFilters };
};

export default useFilter;