import classNames from 'classnames';
import PropTypes from 'prop-types';
import styles from './styles.module.css';

const FilterComponent = ({ filters, handleFilters }) => {
    return (
        <div className={styles.filter__container}>
            {filters.map((filter, index) => (
                <div className={classNames('form-check', styles.filter__group)} key={filter.title} >
                    <label className={classNames('form-check-label', styles.filter__label)} htmlFor={filter.title}>{filter.label}</label>
                    <input className={classNames('form-check-input', styles.filter__input)} type="checkbox" checked={filter.isOn} id={filter.title} onChange={handleFilters} data-filter-id={index} />
                </div>
            ))}
        </div>
    );
};

FilterComponent.propTypes = {
    filters: PropTypes.arrayOf(PropTypes.shape({
        title: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired,
        isOn: PropTypes.bool.isRequired,
    })),
    handleFilters: PropTypes.func.isRequired,
};

export default FilterComponent;