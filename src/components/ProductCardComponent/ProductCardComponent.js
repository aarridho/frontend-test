import Image from 'next/image';
import Link from 'next/link';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import useWishlist from '@hooks/useWishlist';
import useProduct from '@hooks/useProduct';
import isOnline from '@utils/isOnline';
import StarRatingComponent from '@components/StarRatingComponent';
import ProductStockLabel from '@components/ProductStockLabel';
import ProductLabelComponent from '@components/ProductLabelComponent';
import ProductImageComponent from '@components/ProductImageComponent';
import WishlistButton from '@components/WishlistButton';
import { toast } from 'react-toastify';
import styles from './styles.module.css';

const ProductCardComponent = ({ index, id: product_id, type, attributes }) => {
    const { product, mutateProduct } = useProduct(Boolean(product_id), { id: product_id, type, attributes });
    const { stock, slug, images = [], name, points, rating, numOfReviews, isNew } = product?.attributes || {};

    const { isWishlist, setIsWishlist, postWishlist } = useWishlist();

    const handleWishlist = async (e) => {
        e.preventDefault();
        const index = e.currentTarget?.dataset?.index;

        const currentWishlist = isWishlist;
        if (!index) return;

        const id = toast.loading('Please wait...');
        const newData = { id: product_id, type, attributes: { ...attributes, isWishlist: !currentWishlist } };
        setIsWishlist(!currentWishlist);
        const [response, error] = await postWishlist(product_id);

        if (isOnline(true)) {
            if (!response?.data?.data || error) {
                toast.update(id, { render: error?.response?.message || error?.message || 'An Error Occured!', type: 'error', isLoading: false, autoClose: 3000 });

                console.log('AAAAAAAAA');

                mutateProduct({ id: product_id, type, attributes: { ...attributes } }, false);
                setIsWishlist(currentWishlist);
            } else {
                toast.update(id, { render: 'Berhasil menambahkan wishlist!', type: 'success', isLoading: false, autoClose: 3000 });
                mutateProduct(null, true);
            }
        } else {
            toast.update(id, { render: 'Kamu masih offline, kami akan menyimpan sementara wishlist kamu', type: 'success', isLoading: false, autoClose: 3000 });
            mutateProduct(newData, false);
        }
    };

    return (
        <figure className={classNames('card', styles.product__card, { [styles.is__sold_out]: stock <= 0 })}>

            <ProductStockLabel stock={stock} className={styles.product__stock} />
            <ProductImageComponent image={images[0]} slug={slug} />
            <div className={styles.card__body}>
                <h5 className={classNames(styles.product__name, 'font-style-1')}>{name}</h5>
                <div className={styles.product__description}>
                    <div className={styles.product__misc}>
                        <div className={styles.product__points}>
                            <Image src="/images/logo-points.svg" width={11} height={11} alt="logo points" quality={100} />
                            <span>{points} poins</span>
                        </div>
                        <div className={styles.product__tooltips}>
                            <StarRatingComponent id={product_id} rating={rating} />
                            <div className={styles.product__reviews}>{numOfReviews} reviews</div>
                        </div>
                    </div>
                    <div className={styles.product__wishlist}>
                        <WishlistButton data-index={index} className={classNames({ [styles.wishlist__active]: isWishlist })} onClick={handleWishlist} />
                    </div>
                </div>
            </div>
            <ProductLabelComponent isNew={isNew} numOfReviews={numOfReviews} rating={rating} />
            <div className={styles.overlay__items}>
                <span>{name}</span>
                <Link href={{
                    pathname: '/products/[product_id]',
                    query: { product_id: product_id },
                }}>
                    <a className="btn btn-outline-light">
                        View detail
                    </a>
                </Link>
            </div>
        </figure >
    );
};

ProductCardComponent.propTypes = {
    index: PropTypes.number,
    id: PropTypes.string,
    type: PropTypes.string,
    attributes: PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
        info: PropTypes.string,
        description: PropTypes.string,
        points: PropTypes.number,
        slug: PropTypes.string,
        stock: PropTypes.number,
        images: PropTypes.arrayOf(PropTypes.string),
        isNew: PropTypes.number,
        rating: PropTypes.number,
        numOfReviews: PropTypes.number,
        isWishlist: PropTypes.number,
    })
};

export default ProductCardComponent;
