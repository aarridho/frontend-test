import PropTypes from 'prop-types';
import Image from 'next/image';
import classNames from 'classnames';
import { useMemo } from 'react';
import roundHalf from '@utils/roundHalf';
import styles from './styles.module.css';

const ProductLabelComponent = ({ containerClassName, isNew, numOfReviews, rating }) => {
    const [label, backgroundImage] = useMemo(() => {
        let label = null;
        let backgroundImage = '';
        const roundRating = roundHalf(rating);
        const isBestSeller = roundRating >= 4 && numOfReviews > 25;

        if (isNew && isBestSeller) {
            label = <><span>Hot</span><span>Item</span></>;
            backgroundImage = '/images/product-hot-label.svg';
        } else if (isNew) {
            label = <span>New</span>;
            backgroundImage = '/images/product-new-label.svg';
        } else if (isBestSeller) {
            label = <><span>Best</span><span>Seller</span></>;
            backgroundImage = '/images/product-best-seller.svg';
        }

        return [label, backgroundImage];
    }, [isNew, numOfReviews, rating]);

    console.log({ isNew, numOfReviews, rating, });

    return (
        <div className={classNames(styles.product__label, containerClassName)}>
            {backgroundImage && <Image src={backgroundImage} width={76} height={80} alt="label product" />}
            <span className={styles.label}>{label}</span>
        </div>
    );
};
ProductLabelComponent.propTypes = {
    containerClassName: PropTypes.string,
    isNew: PropTypes.number,
    numOfReviews: PropTypes.number,
    rating: PropTypes.number,
};

export default ProductLabelComponent;