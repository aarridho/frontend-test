import PropTypes from 'prop-types';
import classNames from 'classnames';
import { useMemo } from 'react';
import styles from './styles.module.css';

const ProductStockLabel = ({ stock, className, redStockClassName, greenStockClassName }) => {
    const [stockLabel, stockClassName] = useMemo(() => {
        let red = redStockClassName ? redStockClassName : styles.stock__red;
        let green = greenStockClassName ? greenStockClassName : styles.stock__green;

        let label = 'Stock < 5';
        let resolveClassName = [styles.product__stock_label, red, className];

        if (stock > 5) {
            label = 'In Stock';
            resolveClassName[1] = green;
        } else if (stock <= 0) {
            label = 'Sold Out';
        }

        return [label, classNames(resolveClassName)];
    }, [className, greenStockClassName, redStockClassName, stock]);

    return <span className={stockClassName}>{stockLabel}</span>;
};

ProductStockLabel.propTypes = {
    stock: PropTypes.number,
    className: PropTypes.string,
    redStockClassName: PropTypes.string,
    greenStockClassName: PropTypes.string,
};

export default ProductStockLabel;