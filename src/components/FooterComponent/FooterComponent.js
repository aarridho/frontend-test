import Image from 'next/image';
import classNames from 'classnames';
import styles from './styles.module.css';

const FooterComponent = () => {
    return (
        <footer className={styles.footer}>
            <div className={styles.footer__splash_before}></div>
            <div className="container">
                <div className="row row-cols-1 row-cols-lg-2 g-4">
                    <div className={classNames('col col-lg-4', styles.footer__social_links)}>
                        <a href="#"><Image src="/images/ig.svg" width={22.68} height={22.68} alt="Instagram" /></a>
                        <a href="#"><Image src="/images/fb.svg" width={22.68} height={22.68} alt="Facebook" /></a>
                        <a href="#"><Image src="/images/twt.svg" width={22.68} height={22.68} alt="Twitter" /></a>
                    </div>
                    <div className={classNames('col col-lg-8 d-flex justify-content-end', styles.footer__misc)}>
                        <span>{'Terms & Condition'}</span>
                        <span>|</span>
                        <span>{'Copyright © 2018. All rights reserved. PT Radya Gita Bahagi'}</span>
                    </div>
                </div>
            </div>
        </footer>
    );
};

export default FooterComponent;