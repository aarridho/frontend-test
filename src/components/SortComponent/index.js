import SortComponent from './SortComponent';
export { default as useSort } from './useSort';

export default SortComponent;