import PropTypes from 'prop-types';
import styles from './styles.module.css';

const SortComponent = ({ handleSort, value }) => {
    return (
        <div className={styles.sort__container}>
            <span>Urutkan</span>
            <select className="form-select" value={value} aria-label="sort" onChange={handleSort}>
                <option value="attributes.isNew">Terbaru</option>
                <option value="attributes.numOfReviews">Review</option>
                <option value="attributes.stock">Stock</option>
            </select>
        </div>
    );
};

SortComponent.propTypes = {
    handleSort: PropTypes.func.isRequired,
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

export default SortComponent;