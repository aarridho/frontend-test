import { useCallback, useState } from 'react';

const useSort = (defaultSort) => {
    const [sort, setSorts] = useState(defaultSort);

    const handleSort = useCallback((e) => {
        const value = e.target.value;

        setSorts(value);
    }, []);

    return { sort, handleSort };
};

export default useSort;