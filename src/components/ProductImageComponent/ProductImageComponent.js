import PropTypes from 'prop-types';
import Image from 'next/image';
import classNames from 'classnames';
import styles from './styles.module.css';

const ProductImageComponent = ({ containerClassName, image, slug }) => {
    return (
        <div className={classNames(styles.product__image, containerClassName)}>
            <div className={styles.image__box}>
                {image && <Image src={image} alt={slug} layout="fill" objectFit="contain" quality={80} />}
                {!image && <h5 className="h-100 w-100">No Image</h5>}
            </div>
        </div>
    );
};

ProductImageComponent.propTypes = {
    containerClassName: PropTypes.string,
    image: PropTypes.string,
    slug: PropTypes.string,
};

export default ProductImageComponent;