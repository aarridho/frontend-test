import PACKAGE_JSON from 'package.json';

export const APP_NAME = process.env.NEXT_PUBLIC_APP_NAME || PACKAGE_JSON.name;
export const APP_URL = process.env.NEXT_PUBLIC_APP_ENV === 'development' ? 'http://localhost:3000' : process.env.NEXT_PUBLIC_APP_URL;
export const API_URL = process.env.NEXT_PUBLIC_API_URL;