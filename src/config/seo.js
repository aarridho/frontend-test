import { APP_URL } from './meta';

const DEFAULT_SEO = {
    canonical: APP_URL,
    openGraph: {
        type: 'website',
        locale: 'en_IE',
        url: APP_URL,
        site_name: 'ACME Company Logo',
    },
};

export default DEFAULT_SEO;