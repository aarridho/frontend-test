import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { SWRConfig } from 'swr';
import { API_URL } from '@lib/api';

const SWRProvider = ({ children }) => {
    const abortController = useRef(null);
    useEffect(() => {
        const controller = new AbortController();
        abortController.current = controller;

        return () => controller.abort();
    }, []);

    const getToken = async () => { };

    return (
        <SWRConfig value={{
            fetcher: async (...params) => {
                try {
                    const resource_url = params instanceof Array ? params[0] : typeof params === 'string' ? params : null;
                    const controller = abortController.current;
                    const accessToken = await getToken(controller.signal.aborted);

                    let requestOptions = {
                        headers: { Authorization: `Bearer ${accessToken}` }
                    };

                    const response = await fetch(API_URL + resource_url, requestOptions);
                    if (!response.ok) throw response;

                    return await response.json();
                } catch (error) {
                    // console.log(error);

                    // error.message is the body
                    const errorParsed = JSON.parse(error.message);
                    error.info = errorParsed;

                    throw error;
                }
            },
            onErrorRetry: async (err, key, config, revalidate, { retryCount }) => {
                // console.log({ key });
                if (retryCount && retryCount >= 3) return;
                if (err.status === 404) return;
                if (err.status === 401) {
                    const controller = abortController.current;
                    const newAccessToken = await getToken(controller.signal.aborted);

                    if (newAccessToken) {
                        await revalidate();
                    }
                    return;
                }

                setTimeout(
                    () =>
                        revalidate({ retryCount: retryCount + 1 }),
                    5000
                );
            }
        }}>
            {children}
        </SWRConfig>
    );
};

SWRProvider.propTypes = {
    children: PropTypes.node,
};

export default SWRProvider;