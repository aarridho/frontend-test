const withPWA = require('next-pwa');
const runtimeCaching = require('next-pwa/cache');

module.exports = withPWA({
    reactStrictMode: true,
    pwa: {
        dest: 'public',
        disable: process.env.NODE_ENV === 'development',
        runtimeCaching,
    },
    images: {
        domains: ['rgbtest.s3.ap-southeast-1.amazonaws.com'],
    }
});
