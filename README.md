This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, you need to install the app:

```bash
yarn install
```

Then, run the development/local environtment server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy with Static Server

`next export` allows you to export your app to static HTML, which can be run standalone without the need of a Node.js server.

```bash
next build
# or
yarn build
```

and then

```bash
npm start
# or
yarn start
```

And your app is ready to be viewed! 🚀

## About Exporting to Static HTML (`next export` / `yarn export`)

Before you run `yarn build` and `yarn export` You need to provide your third party image loader (e.g: imgix, akamai, cloudinary) to export the application into static html

For example, if you have a third party image loader from imgix:

```JavaScript
// File is: next.config.js
// ...
module.exports = {
    images: {
        domains: ['rgbtest.s3.ap-southeast-1.amazonaws.com'],
        // you need to set this into your provider
        loader: 'imgix',
        path: 'https://<account>.imgix.net' //this is your image loader url path
    }
};
```

If you are using `npm start` or `yarn start`, you don't need to provide loader and path

Or if you are hosting on [Vercel](https://vercel.com), it includes built in image loader and optimization 🚀

If you want to export the application to static html, you could run this command:

```bash
next export
# or
yarn export
```

Then you'll have a static version of your app in the out directory.
By default, `next export` will generate an out directory, which can be served by any static hosting service or CDN.

For environments using Node, the easiest way to handle this would be to install `serve` and let it handle the rest:

```bash
npm install -g serve
serve -s out
```

The last command shown above will serve your static site on the port 5000. Like many of serve’s internal settings, the port can be adjusted using the -l or --listen flags:

```bash
serve -s out -l 4000
```

Run this command to get a full list of the options available:

```
serve -h
```

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

If you haven’t already done so, push your Next.js app to a Git provider of your choice: GitHub, GitLab, or BitBucket. Your repository can be private or public.

Then, follow these steps:

1. Sign up to Vercel (no credit card is required).
2. After signing up, you’ll arrive on the “Import Project” page. Under “From Git Repository”, choose the Git provider you use and set up an integration. (Instructions: GitHub / GitLab / BitBucket).
3. Once that’s set up, click “Import Project From …” and import your Next.js app. It auto-detects that your app is using Next.js and sets up the build configuration for you. No need to change anything — everything should work fine!
4. After importing, it’ll deploy your Next.js app and provide you with a deployment URL. Click “Visit” to see your app in production.
   Congratulations! You’ve deployed your Next.js app! If you have questions, take a look at the Vercel documentation.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.

You can deploy a fresh React project, with a Git repository set up for you, with the following Deploy Link: [Deploy](https://vercel.com/import/git?s=https%3A%2F%2Fgithub.com%2Fvercel%2Fvercel%2Ftree%2Fmaster%2Fexamples%2Fcreate-react-app)

## Why Next.js?

In terms of familiarity, I'm more familiar with React Library and Next.js seems like a good use for SEO based web app

## Best Practice

For me, modular component based app is a good practice for “Don't Repeat Yourself!”. And is a good reason for Reusing the component on another page

React Hooks is the future! No more Class Component! 😻

Using Library like SWR is a good choice for caching and revalidating your fetch data (it makes data and UI more optimistic)

Not Sure if PWA is working (i've never implement PWA before) but i have already planted PWA inside

Using meta and openGraph on web pages is useful for SEO

I've added Product JSON-LD formatted for Google Crawler too

## Additonal Notes

Addition to the app:

- Adjust Navbar Company Logo on home page (list of product page) with extra padding
- Added load more button (infinite data) for products page
- Best Practice using open graph for every product page and Product JSON-LD for Google Crawlers
- Simply not adding label (best-seller, hot-items, new) on product page :)
- Filter and sort is useable!
- Using next js getStaticProps and getStaticPaths for blazing fast SEO loads [More info about getStaticProps and getStaticPaths](https://nextjs.org/docs/basic-features/data-fetching)
