import PropTypes from 'prop-types';
import { APP_NAME, APP_URL } from '@config/meta';
import { NextSeo } from 'next-seo';
import { useRouter } from 'next/dist/client/router';
import LayoutComponent from '@components/LayoutComponent';
import classNames from 'classnames';
import styles from '@styles/products.module.css';
import FilterComponent, { useFilter } from '@components/FilterComponent';
import SortComponent, { useSort } from '@components/SortComponent';
import useProducts from '@hooks/useProducts';
import ProductCardComponent from '@components/ProductCardComponent/ProductCardComponent';
import { useMemo } from 'react';
import roundHalf from '@utils/roundHalf';
import get from 'lodash.get';
import orderBy from 'lodash.orderby';
import filter from 'lodash.filter';
import { ToastContainer } from 'react-toastify';
import deepFlatten from '@utils/deepFlatten';

const defaultFilters = [
    {
        title: 'rating__4+',
        label: 'Rating 4 ke atas',
        condition: (rating) => roundHalf(rating) >= 4,
        value_label: 'attributes.rating',
        isOn: false,
    },
    {
        title: 'ready_stock',
        label: 'Stock Tersedia',
        condition: (stock) => stock > 0,
        value_label: 'attributes.stock',
        isOn: false,
    }
];

const Products = () => {
    const router = useRouter();
    console.log({ router });

    const { products, size, setSize,  /* loading */ } = useProducts(true, { page: 1, size: 9 });

    const { filters, handleFilters } = useFilter(defaultFilters);
    const { sort, handleSort } = useSort('attributes.isNew');

    const mapProducts = useMemo(() => {
        // thanks lodash :')
        const filteredFilters = filter(filters, { isOn: true });

        let formattedProducts = [];
        for (const productsData of products) {
            formattedProducts.push(productsData?.data);
        }

        console.log({ formattedProducts });

        const flattenedProducts = deepFlatten(formattedProducts);
        const sortedProducts = orderBy(flattenedProducts, [sort], ['desc']);
        const filteredProducts = filter(sortedProducts, (product) => {
            for (const filter of filteredFilters) {
                const value = get(product, filter.value_label);
                if (!(filter.condition(value))) return false;
            }

            return true;
        });

        // console.log({ /* sortedProducts, */ filteredFilters, filteredProducts });

        return filteredProducts.map((product, i) => (
            <div key={`product_${product.id}`} className="col">
                <ProductCardComponent
                    {...product}
                    index={i}
                />
            </div>
        ));
    }, [sort, filters, products]);

    return (
        <LayoutComponent>
            <div className={classNames('container-fluid', styles.container)}>
                <div className={styles.content}>
                    <section id={styles['filter-products']}>
                        <div className={styles.content__header}>
                            <h5 className={styles.content__title}>Filter</h5>
                        </div>
                        <FilterComponent filters={filters} handleFilters={handleFilters} />
                    </section>
                    <section id={styles.products}>
                        <div className={styles.content__header}>
                            <h5 className={styles.content__title}>Product List</h5>
                            <SortComponent value={sort} handleSort={handleSort} />
                        </div>
                        <div className={classNames('row row-cols-1 row-cols-md-3 g-4', styles.product_list)}>
                            {mapProducts}
                        </div>
                        <div className="d-flex justify-content-center">
                            <button type="button" onClick={() => setSize(size + 1)} className="btn btn-outline-info btn-md">Load More</button>
                        </div>
                    </section>
                </div>
            </div>
            <ToastContainer
                position="bottom-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                style={{ zIndex: '9999' }}
            />
        </LayoutComponent>
    );
};

const Page = ({ title }) => {
    return (
        <>
            <NextSeo
                title={title}
                description={`Learn more about ${title}`}
                openGraph={{
                    title: `${title} | ${APP_NAME}`,
                    description: `Learn more about ${title}`,
                    url: APP_URL,
                    type: 'website'
                }}
            />
            <Products />
        </>
    );
};

Page.propTypes = {
    title: PropTypes.string,
    gifts_id: PropTypes.number,
};

export async function getStaticProps() {
    return {
        props: {
            title: 'Product List'
        }
    };
}

export default Page;
