import Link from 'next/link';
import Image from 'next/image';
import PropTypes from 'prop-types';
import { APP_NAME, APP_URL } from '@config/meta';
import { NextSeo, ProductJsonLd } from 'next-seo';
import { getProducts } from '@hooks/useProducts';
import useProduct, { getProductById, PRODUCT_PROPTYPES } from '@hooks/useProduct';
import formatError from '@utils/formatError';
import LayoutComponent from '@components/LayoutComponent';
import classNames from 'classnames';
import styles from '@styles/product.module.css';
import ProductImageComponent from '@components/ProductImageComponent';
import StarRatingComponent from '@components/StarRatingComponent';
import ProductStockLabel from '@components/ProductStockLabel';
import { useEffect, useMemo, useState, } from 'react';
import useCounter from '@hooks/useCounter';
import convertBr from '@utils/convertBr';

const Product = ({ defaultProduct }) => {
    const { product, /* loading */ } = useProduct(true, defaultProduct);
    const { counter, increase, decrease } = useCounter(0);

    const { id, attributes: { name, images, slug, rating, numOfReviews, points, stock, info, description } } = product;
    const pointsWithDots = useMemo(() => points.toLocaleString('id-ID'), [points]);
    const convertedDesc = useMemo(() => convertBr(description), [description]);

    const [isClient, setIsClient] = useState(false);
    useEffect(() => {
        setIsClient(true);
    }, []);

    return (
        <LayoutComponent>
            <div className={classNames('container', styles.container)}>
                <nav className={styles.bread__crumb} aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item">
                            <Link href="/products">
                                <a>List product</a>
                            </Link>
                        </li>
                        <li className="breadcrumb-item active" aria-current="page">
                            <Link href={`/products/${id}`}>
                                <a>
                                    {name}
                                </a>
                            </Link>
                        </li>
                    </ol>
                </nav>
                <div className={styles.content}>
                    <div className="row row-cols-1 row-cols-md-2 g-3 mb-5 justify-content-between">
                        <div className={classNames('col col-md-7 card', styles.image__wrapper)}>
                            <ProductImageComponent containerClassName={styles.product__image} image={images[0]} slug={slug} />
                        </div>
                        <div className="col col-md-5">
                            <div className={styles.product__detail}>
                                <h3>{name}</h3>
                                <div className={styles.product__rating}>
                                    <StarRatingComponent id={id} containerClassName={styles.star__rating} rating={rating} width={16.79} height={16} />
                                    <span className={styles.product__reviews}>{numOfReviews} reviews</span>
                                </div>
                                <div className={styles.product__info}>
                                    <span className={styles.product__points}>
                                        <Image src="/images/point-give-details.svg" width={19.96} height={20} alt="point-give-details" quality={100} />
                                        <span>{pointsWithDots} poins</span>
                                    </span>
                                    <ProductStockLabel stock={stock} className={styles.product__stock} greenStockClassName={styles.stock__green} />
                                </div>
                                <span className={styles.product__about} dangerouslySetInnerHTML={{ __html: info }}>
                                </span>
                                <div className={styles.product__counter}>
                                    <span>Jumlah</span>
                                    <div>
                                        <button type="button" className="btn btn-light" onClick={decrease}>
                                            <span>
                                                <Image src="/images/minus.svg" width={10} height={10} alt="minus" />
                                            </span>
                                        </button>
                                        <span>{counter}</span>
                                        <button type="button" className="btn btn-light" onClick={increase}>
                                            <span>
                                                <Image src="/images/plus.svg" width={10} height={10} alt="plus" />
                                            </span>
                                            <span>
                                                <Image src="/images/minus.svg" width={10} height={10} alt="plus" />
                                            </span>
                                        </button>
                                    </div>
                                </div>
                                <div className={styles.product__buttons}>
                                    <button type="button" className={classNames('btn', styles.wishlist__button)}>
                                        <Image src="/images/btn-wishlist-lg.svg" layout="fill" objectFit="cover" alt="wishlist button" />
                                    </button>
                                    <button type="button" className={classNames('btn btn-success', styles.redeem__button)}>
                                        Redeem
                                    </button>
                                    <button type="button" className={classNames('btn btn-outline-success', styles.cart__button)}>
                                        Add to cart
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <nav>
                            <div className={classNames('nav nav-tabs', styles.nav__tabs)} id="nav-tab" role="tablist">
                                <button className="nav-link active" id="nav-home-tab" type="button" role="tab" aria-controls="nav-home" aria-selected="true">Info Produk</button>
                            </div>
                        </nav>
                        <div className={classNames('tab-content', styles.tab__content)} id="nav-tabContent">
                            <div className={classNames('tab-pane fade show active', styles.tab__pane)} id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <h4>Rincian</h4>
                                {isClient && <div dangerouslySetInnerHTML={{ __html: convertedDesc }}></div>}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </LayoutComponent>
    );
};

Product.propTypes = {
    defaultProduct: PRODUCT_PROPTYPES,
};

const Page = ({ product, meta }) => {
    return (
        <>
            <NextSeo
                title={meta?.title}
                type={meta?.type}
                openGraph={{
                    url: meta?.url,
                    title: meta?.title,
                    description: meta?.description,
                    images: meta?.images,
                    site_name: APP_NAME
                }}
            />
            <ProductJsonLd
                productName={meta?.productName}
                description={meta?.description}
                images={meta?.images}
                manufacturerName={APP_NAME}
                aggregateRating={meta?.aggregateRating}
            />
            <Product defaultProduct={product} />
        </>
    );
};

Page.propTypes = {
    product: PRODUCT_PROPTYPES,
    meta: PropTypes.shape({
        title: PropTypes.string,
        productName: PropTypes.string,
        description: PropTypes.string,
        type: PropTypes.string,
        images: PropTypes.arrayOf(PropTypes.string),
        url: PropTypes.string,
        aggregateRating: PropTypes.shape({
            ratingValue: PropTypes.number,
            reviewCount: PropTypes.number,
        })
    }),
};

export async function getStaticProps({ params: { product_id } } = {}) {
    const [res, error] = await getProductById(product_id);
    if (error) throw formatError('An Error occured at build time (getStaticProps)', { message: error?.message, error });

    const product = res?.data?.data;
    // console.log({ product });
    const { type } = product;
    const { name, description, images, rating, numOfReviews } = product?.attributes;
    const title = `Product ${name}`;
    const url = APP_URL + '/' + product_id;

    return {
        props: {
            product,
            meta: {
                title,
                productName: name,
                description: description,
                images: images,
                url,
                type,
                aggregateRating: {
                    ratingValue: rating,
                    reviewCount: numOfReviews
                }
            }
        },
    };
}

// we are setting for the max of items, to gather all data
// because of this, the build time does take much longer (because of how much data it needs to gather first)
const TOTAL_ITEMS = 20;
const PAGE = 1;
export async function getStaticPaths() {
    const [res, error] = await getProducts({ page: PAGE, size: TOTAL_ITEMS });
    if (error) throw formatError('An Error occured at build time (getStaticPaths)', { message: error?.message, error });

    const products = res?.data?.data;
    const paths = products.map(product => {
        return {
            params: {
                product_id: product.id,
            }
        };
    });

    return {
        paths,
        fallback: false,
    };
}

export default Page;
