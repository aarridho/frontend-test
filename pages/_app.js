import Head from 'next/head';
import { APP_NAME } from '@config/meta';
import PropTypes from 'prop-types';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';
import '@styles/globals.css';
import { DefaultSeo } from 'next-seo';
import DEFAULT_SEO from '@config/seo';

function MyApp({ Component, pageProps }) {
    return (
        <>
            <Head>
                <meta name='viewport' content='minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no, user-scalable=yes, viewport-fit=cover' />
            </Head>
            <DefaultSeo
                {...DEFAULT_SEO}
                titleTemplate={`%s | ${APP_NAME}`}
                defaultTitle={APP_NAME}
            />
            <Component {...pageProps} />
        </>
    );
}

MyApp.propTypes = {
    Component: PropTypes.elementType,
    pageProps: PropTypes.object
};


export default MyApp;
